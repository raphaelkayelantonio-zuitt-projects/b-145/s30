console.log('ola');



// 1
async function fetchData(){

	
	let result = await fetch('https://jsonplaceholder.typicode.com/todos')
	let json = await result.json();

	console.log(json);
}

fetchData();

// 6
fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log('The item ' + json.title + " on the list has a statue of " + json.status + " script"))

// 7

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		id: 201,
		title: 'Created to do list item',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 8

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: 'Pending',
		description: 'To update the my to do list with a different structure',
		id: 1,
		status: 'Pending',
		title: 'Updated to do list item',
		userId: 1
	})

})
.then((response) => response.json())
.then((json) => console.log(json));

// 9

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PATCH',
	headers: {
		'Content-Type' : 'application/json'
	},
	body: JSON.stringify({
		completed: false,
		dateCompleted: '07/09/21',
		id: 1, 
		status: 'Complete',
		title: 'delectus aut autem',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// 10
fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'DELETE'
});

